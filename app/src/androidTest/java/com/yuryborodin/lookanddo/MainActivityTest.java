package com.yuryborodin.lookanddo;

import android.content.Intent;
import android.view.View;

import androidx.test.annotation.UiThreadTest;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest{
    Intent intent;
    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule =
            new ActivityTestRule<MainActivity>(MainActivity.class);
    private MainActivity mainActivity = null;
    @Before
    public void setUp() throws Exception{
        mainActivity = mainActivityTestRule.getActivity();
    }
    @Test
    public void testLaunch(){
        View mainTextView1 = mainActivity.findViewById(R.id.mainTextView);
        View mainTextView2 = mainActivity.findViewById(R.id.scoreView);
        View mainClearButton = mainActivity.findViewById(R.id.clearButton);
        View mainProceedButton = mainActivity.findViewById(R.id.proceedButton);
        View mainUpdateButton = mainActivity.findViewById(R.id.updateButton);
        View mainImageView = mainActivity.findViewById(R.id.imageView);
        View mainPaintView = mainActivity.findViewById(R.id.drawView);

        assertNotNull(mainTextView1);
        assertNotNull(mainTextView2);
        assertNotNull(mainClearButton);
        assertNotNull(mainProceedButton);
        assertNotNull(mainUpdateButton);
        assertNotNull(mainImageView);
        assertNotNull(mainPaintView);
        assertEquals(mainActivity.getCurrentImage(), 0);


    }

    @After
    public void tearDown(){
        mainActivity = null;
    }

    @Before
    @After
    public void cleanSharedPrefs(){

    }
    @Test
    public void onCreate() {
        int testValueCurrentImage = mainActivity.getCurrentImage();
        int testValueCurrentStep = mainActivity.getCurrentStep();
        int testValueCurrentSubStep = mainActivity.getCurrentSubStep();
        int testValueCurrentTag = mainActivity.getCurrentTag();
        double testValueTotalScore = mainActivity.getTotalScore();



        assertEquals(testValueCurrentImage,0);
        assertEquals(testValueCurrentStep,0);
        assertEquals(testValueCurrentSubStep,0);
        assertEquals(testValueCurrentTag,0);
        assertEquals(testValueTotalScore,0,0);
    }

    @Test
    public void analyse() {
    }

    @UiThreadTest
    @Test
    public void clickTest(){
        View mainUpdateButton = mainActivity.findViewById(R.id.updateButton);
        int testValueCurrentTag = mainActivity.getCurrentTag();
        mainUpdateButton.performClick();

    }
    @Test
    public void showMessageAfterLoading() {
    }

    @Test
    public void proceedToAnalysis() {


    }

    @Test
    public void showInitialMessage() {
    }

    @Test
    public void onCreateOptionsMenu() {
    }

    @Test
    public void onOptionsItemSelected() {
    }

    @Test
    public void onClearButtonClicked() {
    }

    @Test
    public void onUpdateButtonClicked() {
    }

    @Test
    public void showInfoMessage() {
    }

    @Test
    public void finalMessage() {
    }

    @Test
    public void newGame() {
    }

    @Test
    public void getIteratorImage() {
    }
}